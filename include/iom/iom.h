#pragma once
#include <string>
#include <iostream>
#include <sstream>
#include <exception>
#include <vector>

// Following preprocessor symbols can be defined
// IOM_NO_EXCEPTIONS---to disable exceptions. Then you have to test state of istream/ostream on your own,
// IOM_NO_CPP11---to disable fancy C++ 11 read/write functions with variable input/output parameters.

namespace iom
{
    struct IoException : public std::exception
    {
        IoException()
            : _message("Unknown I/O Exception")
        {}

        explicit IoException(const std::string& errorMessage)
            : _message(errorMessage)
        {}

        virtual ~IoException();

        virtual const char* what() const _NOEXCEPT
        { return _message.c_str(); }

    protected:
        void setMessage(const std::string& msg)
        { _message = msg; }

    private:
        std::string _message;

    };

    struct EofException : public IoException
    {
        EofException()
            : IoException("End Of File Exception")
        {}

        explicit EofException(const std::string& errorMessage)
            : IoException(errorMessage)
        {}

        virtual ~EofException();
    };

    struct FixedValueException : public IoException
    {
        template <typename T>
        FixedValueException(T expectedValue, T gotValue)
        {
            std::ostringstream oss;
            oss << "Fixed Value Error: Expecting '" << expectedValue << "' but got '" << gotValue << "'";
            setMessage(oss.str());
        }

    };

    struct ExcessiveArraySizeException : public IoException
    {
        ExcessiveArraySizeException(int limit, int actualSize)
            : _limit(limit), _actualSize(actualSize)
        {
            std::ostringstream oss;
            oss << "Excessive Array Size: Array size (" << actualSize << ") exceeds allowed limit (" << limit << ")";
            setMessage(oss.str());

        }

        int limit() const
        { return _limit; }

        int actualSize() const
        { return _actualSize; }

    private:
        int _limit;
        int _actualSize;
    };

    template <typename T>
    struct IoUtils
    {
        static T read(std::istream& is)
        {
            T ret;
            is.read(reinterpret_cast<char*>(&ret), sizeof(T));

            if (!is.good())
            {
                if (is.eof())
                    throw EofException();
                else
                    throw IoException();
            }
            return ret;
        }

        static void write(std::ostream& os, const T& val)
        {
            os.write(reinterpret_cast<const char*>(&val), sizeof(T));
            if (!os.good())
                throw IoException();
        }
    };

    template <>
    struct IoUtils<bool>
    {

        static bool read(std::istream& is)
        {
            return IoUtils<char>::read(is) != 0;
        }

        static void write(std::ostream& os, const bool& val)
        {
            IoUtils<char>::write(os, val);
        }
    };

    long long readLeb128(std::istream& is);

    void writeLeb128(std::ostream& os, long long val);

    template <>
    struct IoUtils<std::string>
    {
        static std::string read(std::istream& is)
        {
            size_t n = size_t(readLeb128(is));
            char* buf = new char[n];
            is.read(buf, n);
            std::string ret(buf, n);
            delete [] buf;
            return ret;
        }

        static void write(std::ostream& os, const std::string& val)
        {
            writeLeb128(os, val.size());
            os.write(val.c_str(), val.size());
        }
    };

    void skip(std::istream& is, int bytes);
    size_t stringStorageSize(const std::string& s);

    template <typename T>
    T read(std::istream& is)
    { return IoUtils<T>::read(is); }

    template <typename T>
    void read(std::istream& is, T& out)
    { out = read<T>(is); }

    template <typename T>
    void write(std::ostream& os, const T& val)
    { IoUtils<T>::write(os, val); }

#ifndef IOM_NO_CPP11
    template <typename ArgT0, typename ... ArgsT>
    void read(std::istream& is, ArgT0& arg0, ArgsT&... args)
    {
        read(is, arg0);
        read(is, args...);
    }

    template <typename ArgT0, typename ... ArgsT>
    void write(std::ostream& os, const ArgT0& arg0, const ArgsT&... args)
    {
        write(os, arg0);
        write(os, args...);
    }
#endif


    template <typename T>
    void writeNullIndices(std::ostream& os, T* vals, size_t num)
    {
        short nullsCount = 0;
        for (int i = 0; i < num; i++) if (vals[i] == nullptr) nullsCount++;
        write(os, nullsCount);
        for (int i = 0; i < num; i++) if (vals[i] == nullptr) write(os, i);
    }

    void readNullIndices(std::istream& is, std::vector<int>& outIndices);

}
