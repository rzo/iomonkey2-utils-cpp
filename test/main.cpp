#include <iostream>
#include <fstream>
#include <string>
#include "iom/iom.h"

using namespace std;

int main()
{
    char b;
    short s;
    int i;
    float f;
    std::string str;

    b = 42;
    s = 42;
    i = 42;
    f = 3.14f;
    str = "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world "
            "Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world ";

    cout << "str size " << str.length() << endl;

    {
        fstream fs("test.dat", ios::out | ios::binary);
        /*
        iom::write(fs, b);
        iom::write(fs, s);
        iom::write(fs, i);
        iom::write(fs, f);
        iom::write(fs, str);
        */
        iom::write(fs, b, s, i, f, str);
        fs.close();
    }

    f = b = s = i = 0;
    str = "";

    {
        ifstream fs("test.dat", ios::in | ios::binary);
        /*
        iom::read(fs, b);
        iom::read(fs, s);
        iom::read(fs, i);
        iom::read(fs, f);
        iom::read(fs, str);
        */
        iom::read(fs, b, s, i, f, str);
        fs.close();
    }
    cout << int(b) << " " << s << " " << i << " " << f << " " << str << endl;
    cout << "str size " << str.length() << endl;

    return 0;
}