set(TEST_SOURCE_FILES main.cpp)

add_executable(iomonkey_utils_cpp ${TEST_SOURCE_FILES})
target_link_libraries(iomonkey_utils_cpp iomutils)