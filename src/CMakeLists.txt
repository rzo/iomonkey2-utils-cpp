set(HEADER_FILES ../include/iom/iom.h)
set(SOURCE_FILES iom/iom.cpp)

add_library(iomutils STATIC ${SOURCE_FILES})
target_include_directories(iomutils PUBLIC ../include)
target_compile_features(iomutils PUBLIC cxx_variadic_templates)
