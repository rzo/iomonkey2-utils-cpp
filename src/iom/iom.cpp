#include "iom/iom.h"

#include <cstdint>

/*
#define IS_BIG_ENDIAN (*((uint16_t*)"\0\xff") < 0x100)
#if ((reinterpret_cast<uint16_t*>("\0\xff"))[0] < 0x100)
#error Big Endian Platforms are not supported
#endif
*/


namespace iom
{
    IoException::~IoException()
    {}

    EofException::~EofException()
    {}

    void skip(std::istream& is, int bytes) 
    {
        is.ignore(bytes);
        if (!is.good())
        {
            if (is.eof())
                throw EofException();
            else
                throw IoException();
        }
    }

    size_t stringStorageSize(const std::string& s)
    {
        int n, ret = n = s.length();

        // LEB 128 for string size prefix
        do { ret++; } while (n >>= 7);
        return ret;
    }

    long long readLeb128(std::istream& is)
    {
        long long ret = 0;
        int b;
        int off = 0;
        do 
        {
            b = IoUtils<char>::read(is);
            ret |= (b & 0x7f) << off;
            off += 7;
        } 
        while ((b & 0x80) != 0);

        return ret;
    }

    void writeLeb128(std::ostream& os, long long val)
    {
        do
        {

            unsigned char b = val & 0x7f;
            val >>= 7;
            if (val)
                b |= 0x80;
            IoUtils<char>::write(os, b);
        }
        while (val);
    }

    void readNullIndices(std::istream& is, std::vector<int>& outIndices)
    {
        short nullsCount; 
        read(is, nullsCount);

        outIndices.clear();
        while (nullsCount--)
        {
            int idx;
            read(is, idx);
            outIndices.push_back(idx);
        } 
    }

}
